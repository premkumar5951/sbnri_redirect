import { Box, HStack, Image, Text, VStack } from "@chakra-ui/react";
import React from "react";

const Couple = () => {
  return (
    <>
      <VStack w="full" align="center"mt="3em" zIndex={50}>
        <Box
          maxW="450px"
          maxH="450px"
          mb="2rem"
          padding="0rem"
          border="25px solid transparent"
          style={{
            borderImageSlice: "43 45 45 41",
            borderImageWidth: "25px 25px 25px 25px",
            borderImageRepeat: "round round",
            borderImageSource:
              "url('https://weddie.netlify.app/img/traditional_wedding_header_frame.png')",
            borderStyle: "solid",
          }}
          __css={{
            "--webkit-border-image":
              "url(https://weddie.netlify.app/img/traditional_wedding_header_frame.png) 45 round",
          }}
        >
          <Image
            src="https://res.cloudinary.com/dxjehekla/image/upload/v1666204504/marriage/WhatsApp_Image_2022-10-19_at_11.30.19_PM_qcnyg7.jpg"
            objectFit={"cover"}
            objectPosition="top"
          />
        </Box>
<VStack w={["90%","80%","70%"]} textAlign={"center"}>
<Text fontSize={["30px","40px","40px"]} color="white" fontWeight={"bold"} letterSpacing="0.3px">Chandni weds Rahul</Text>
        <Text fontSize={["20px","24px","24px"]} color="white" fontWeight={"semibold"} letterSpacing="2px" style={{"margin":"1rem 0px !important"}}>Together with our families, We invite you to share this day of happiness</Text>
</VStack>
        

<Box w="80%" style={{"margin":"2rem 0px !important"}} h="100px" backgroundPosition={"center"} backgroundImage={"https://weddie.netlify.app/img/traditional_arch_border.png"} backgroundRepeat="repeat-x" />
      </VStack>
    </>
  );
};

export default Couple;
