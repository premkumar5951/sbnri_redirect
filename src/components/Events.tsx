import { Box, HStack, Icon, Image, Text, VStack } from "@chakra-ui/react";
import React from "react";
import { GiDiamondRing } from "react-icons/gi";
import { BsStopwatch } from "react-icons/bs";
import { SlCalender } from "react-icons/sl";

const Events = () => {
  return (
    <>
      <VStack w="full" align="center" id="events">
        <Text
          fontSize={["30px", "40px", "40px"]}
          color="white"
          fontWeight={"bold"}
          letterSpacing="0.3px"
          mb="3rem"
          _after={{
            content: "' '",
            width: "100%",
            marginTop: "3px",
            height: "2px",
            display: "block",
            background: "white",
          }}
        >
          Main Events
        </Text>
        <HStack w="full" flexWrap={"wrap"} justify="center" align="center">
        <Box
          maxW="450px"
          padding="0rem"
          border="25px solid transparent"
          style={{
            margin:"0.5rem !important",
            borderImageSlice: "43 45 45 41",
            borderImageWidth: "25px 25px 25px 25px",
            borderImageRepeat: "round round",
            borderImageSource:
              "url('https://weddie.netlify.app/img/traditional_wedding_header_frame.png')",
            borderStyle: "solid",
          }}
          __css={{
            "--webkit-border-image":
              "url(https://weddie.netlify.app/img/traditional_wedding_header_frame.png) 45 round",
          }}
        >
          
          <VStack w="full" align="center">
            <Text
              textAlign="center"
              fontSize="32px"
              w="95%"
              color="white"
              p="12px"
              mb={"1rem"}
              borderBottom={"1px solid white"}
              fontWeight={"bold"}
              letterSpacing="0.3px"
            >
              Haldi & Mehndi
            </Text>
            <HStack
              w="full"
              justify={"space-between"}
              align="flex-start"
              pt="0.8rem"
            >
              <VStack
                flex="1"
                align="center"
                justify={"center"}
                px="1rem"
                spacing={5}
              >
                <Icon as={SlCalender} color="white" width="25px" h="25px" />
                <Text
                  fontSize="17px"
                  textAlign="center"
                  letterSpacing={"1px"}
                  fontWeight={"bold"}
                  color="white"
                >
                  Thu,&nbsp; 24&nbsp; Nov,&nbsp; 2022
                </Text>
              </VStack>
            </HStack>
            <Box w="full" p="1rem">
              <Text
                textAlign={"center"}
                fontSize="20px"
                color="white"
                fontWeight={"normal"}
                letterSpacing="1px"
              >
                Nirmaya Hall, Sahibganj, Jharkhand 816109
              </Text>
            </Box>
          </VStack>
        </Box>
        <Box
          maxW="450px"
          padding="0rem"
          border="25px solid transparent"
          style={{
            margin:"0.5rem !important",
            borderImageSlice: "43 45 45 41",
            borderImageWidth: "25px 25px 25px 25px",
            borderImageRepeat: "round round",
            borderImageSource:
              "url('https://weddie.netlify.app/img/traditional_wedding_header_frame.png')",
            borderStyle: "solid",
          }}
          __css={{
            "--webkit-border-image":
              "url(https://weddie.netlify.app/img/traditional_wedding_header_frame.png) 45 round",
          }}
        >
          
          <VStack w="full" align="center">
            <Text
              textAlign="center"
              fontSize="32px"
              w="95%"
              color="white"
              p="12px"
              mb={"1rem"}
              borderBottom={"1px solid white"}
              fontWeight={"bold"}
              letterSpacing="0.3px"
            >
              Wedding
            </Text>
            <HStack
              w="full"
              justify={"space-between"}
              align="flex-start"
              pt="0.8rem"
            >
              <VStack
                flex="1"
                px="1rem"
                align="center"
                justify={"center"}
                spacing={5}
              >
                <Icon as={BsStopwatch} color="white" width="25px" h="25px" />
                <Text
                  display="block"
                  mt="1rem"
                  fontSize="17px"
                  textAlign="center"
                  letterSpacing={"1px"}
                  color="white"
                  fontWeight={"bold"}
                >
                  07:00 PM Onwards
                </Text>
              </VStack>
              <VStack
                flex="1"
                align="center"
                justify={"center"}
                px="1rem"
                spacing={5}
              >
                <Icon as={SlCalender} color="white" width="25px" h="25px" />
                <Text
                  fontSize="17px"
                  textAlign="center"
                  letterSpacing={"1px"}
                  fontWeight={"bold"}
                  color="white"
                >
                  Fri,&nbsp; 25&nbsp; Nov,&nbsp; 2022
                </Text>
              </VStack>
            </HStack>
            <Box w="full" p="1rem">
              <Text
                textAlign={"center"}
                fontSize="20px"
                color="white"
                fontWeight={"normal"}
                letterSpacing="1px"
              >
                Nirmaya Hall, Sahibganj, Jharkhand 816109
              </Text>
            </Box>
          </VStack>
        </Box>
        </HStack>

        <Box
          w="80%"
          style={{ margin: "2rem 0px !important" }}
          mt="3rem"
          h="100px"
          backgroundPosition={"center"}
          backgroundImage={
            "https://weddie.netlify.app/img/traditional_arch_border.png"
          }
          backgroundRepeat="repeat-x"
        />
      </VStack>
    </>
  );
};

export default Events;
