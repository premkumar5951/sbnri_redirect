import { Box, HStack, Icon, Image, Text, VStack } from "@chakra-ui/react";
import React from "react";
import { GiLovers } from "react-icons/gi";
import { TbCircleDotted } from "react-icons/tb";
import { GiSelfLove } from "react-icons/gi";
import { GiDiamondRing } from "react-icons/gi";

const Story = () => {
  return (
    <>
      <VStack w="full" align="center" display={["none", "none", "flex"]}>
        <VStack w="70%" textAlign={"center"}>
          <Text
            color="white"
            fontWeight={"bold"}
            letterSpacing="0.3px"
            fontSize={["30px", "40px", "40px"]}
            _after={{
              content: "' '",
              width: "100%",
              marginTop: "3px",
              height: "2px",
              display: "block",
              background: "white",
              marginBottom: "1rem",
            }}
          >
            Our Story...
          </Text>
          <Text
            fontSize={["20px", "24px", "24px"]}
            color="white"
            fontWeight={"bold"}
            letterSpacing="0.3px"
          >
            Once in a while, right in the middle of an ordinary life, love gives
            us a fairy tale.
          </Text>
        </VStack>
        <HStack
          w="full"
          align="center"
          pos="relative"
          style={{ margin: "2rem 0px" }}
        >
          <HStack flex="1" align="center" justify="flex-end">
            <VStack>
              <VStack
                w={["275px", "335px", "425px"]}
                bg="#F57C00"
                pos="relative"
                right="15%"
                p="35px"
                rounded={"1.5rem"}
              >
                <Text
                  fontSize="20px"
                  fontWeight={900}
                  color="white"
                  _after={{
                    content: "' '",
                    width: "100%",
                    marginTop: "3px",
                    height: "2px",
                    display: "block",
                    background: "white",
                  }}
                >
                  ENGAGEMENT
                </Text>
                <Text
                  fontSize="17px"
                  letterSpacing={"1px"}
                  fontWeight={"bold"}
                  color="white"
                >
                  Sat,&nbsp; 11&nbsp; June,&nbsp; 2022
                </Text>
                <Text
                  fontSize="14px"
                  textAlign="center"
                  fontWeight={"light"}
                  color="white"
                >
                  Some knots are meant to be tied forever. Here is our sweet and
                  smooth journey towards eternity holding each other's hand. Two
                  souls but a single thought; two hearts that started beating as
                  one.
                </Text>
                <HStack
                  bg="#F57C00"
                  justify={"Center"}
                  w="100px"
                  h="100px"
                  pos="absolute"
                  bottom={0}
                  right="-28%"
                  roundedTop={"20px"}
                  roundedBottomRight={"20px"}
                  zIndex={10}
                >
                  <Icon
                    as={GiDiamondRing}
                    color="white"
                    width="35px"
                    h="35px"
                  />
                  <Icon
                    as={TbCircleDotted}
                    color="white"
                    width="100px"
                    h="100px"
                    pos="absolute"
                    style={{ margin: "0px !important" }}
                  />
                </HStack>
                <Box
                  bg="#F57C00"
                  w="100px"
                  h="20px"
                  pos="absolute"
                  bottom={0}
                  right="-10%"
                />
                <Box
                  bg="#282262"
                  w="19px"
                  h="19px"
                  pos="absolute"
                  rounded="full"
                  bottom={"12px"}
                  right="-19px"
                />
              </VStack>
            </VStack>
          </HStack>
          <VStack
            flex="1"
            spacing={"1em"}
            minH="600px"
            align="flex-start"
            justifyContent={"space-between"}
          >
            <VStack py="1rem">
              <VStack
                maxW="425px"
                bg="#03A9F4"
                pos="relative"
                left="15%"
                p="35px"
                rounded={"1.5rem"}
              >
                <Text
                  fontSize="20px"
                  fontWeight={900}
                  color="white"
                  _after={{
                    content: "' '",
                    width: "100%",
                    marginTop: "3px",
                    height: "2px",
                    display: "block",
                    background: "white",
                  }}
                >
                  FIRST WE MET
                </Text>
                <Text
                  fontSize="17px"
                  letterSpacing={"1px"}
                  fontWeight={"bold"}
                  color="white"
                >
                  Wed,&nbsp; 25&nbsp; Dec,&nbsp; 2019
                </Text>
                <Text
                  fontSize="14px"
                  textAlign="center"
                  fontWeight={"light"}
                  color="white"
                >
                  If we could only control the time,
                  We would go back to the moment when we both first met, not to
                  change anything, but to experience it all over again!
                </Text>
                <HStack
                  bg="#03A9F4"
                  justify={"Center"}
                  w="100px"
                  h="100px"
                  pos="absolute"
                  bottom={0}
                  left="-28%"
                  roundedTop={"20px"}
                  roundedBottomLeft={"20px"}
                  zIndex={10}
                >
                  <Icon as={GiLovers} color="white" width="35px" h="35px" />
                  <Icon
                    as={TbCircleDotted}
                    color="white"
                    width="100px"
                    h="100px"
                    pos="absolute"
                    style={{ margin: "0px !important" }}
                  />
                </HStack>
                <Box
                  bg="#03A9F4"
                  w="100px"
                  h="20px"
                  pos="absolute"
                  bottom={0}
                  left="-10%"
                />
                <Box
                  bg="#282262"
                  w="19px"
                  h="19px"
                  pos="absolute"
                  rounded="full"
                  bottom={"12px"}
                  left="-19px"
                />
              </VStack>
            </VStack>
            <VStack>
              <VStack
                maxW="425px"
                bg="#D6136A"
                pos="relative"
                p="35px"
                left="15%"
                rounded={"1.5rem"}
              >
                <Text
                  fontSize="20px"
                  fontWeight={900}
                  color="white"
                  _after={{
                    content: "' '",
                    width: "100%",
                    marginTop: "3px",
                    height: "2px",
                    display: "block",
                    background: "white",
                  }}
                >
                  WEDDING
                </Text>
                <Text
                  fontSize="17px"
                  letterSpacing={"1px"}
                  fontWeight={"bold"}
                  color="white"
                >
                  Fri,&nbsp; 25&nbsp; Nov,&nbsp; 2022
                </Text>
                <Text
                  fontSize="14px"
                  textAlign="center"
                  fontWeight={"light"}
                  color="white"
                >
                  The best part of the life is spending each and every day with the fiance, Going on a date, shopping wedding dress, preparing wedding vows with the excitement and eagerness of waiting for the big day!
                </Text>
                <HStack
                  bg="#D6136A"
                  justify={"Center"}
                  w="100px"
                  h="100px"
                  pos="absolute"
                  bottom={0}
                  left="-28%"
                  roundedTop={"20px"}
                  roundedBottomLeft={"20px"}
                  zIndex={10}
                >
                  <Icon as={GiSelfLove} color="white" width="35px" h="35px" />
                  <Icon
                    as={TbCircleDotted}
                    color="white"
                    width="100px"
                    h="100px"
                    pos="absolute"
                    style={{ margin: "0px !important" }}
                  />
                </HStack>
                <Box
                  bg="#D6136A"
                  w="100px"
                  h="20px"
                  pos="absolute"
                  bottom={0}
                  left="-10%"
                />
                <Box
                  bg="#282262"
                  w="19px"
                  h="19px"
                  pos="absolute"
                  rounded="full"
                  bottom={"12px"}
                  left="-19px"
                />
              </VStack>
            </VStack>
          </VStack>
          <Box
            w="10px"
            h="350px"
            top="30%"
            bg="#95A5A5"
            pos="absolute"
            left="50%"
            style={{ margin: "0px !important" }}
            transform="translateX(-50%)"
          />
        </HStack>

        <Box
          w="80%"
          style={{ margin: "2rem 0px !important" }}
          h="100px"
          backgroundPosition={"center"}
          backgroundImage={
            "https://weddie.netlify.app/img/traditional_arch_border.png"
          }
          backgroundRepeat="repeat-x"
        />
      </VStack>

      {/* ______________________________ */}

      <VStack
        w="full"
        align="center"
        display={["flex", "flex", "none"]}
        id="story"
      >
        <VStack w={["90%", "80%"]} textAlign={"center"}>
          <Text
            color="white"
            fontWeight={"bold"}
            letterSpacing="0.3px"
            fontSize={["30px", "40px", "40px"]}
            _after={{
              content: "' '",
              width: "100%",
              marginTop: "3px",
              height: "2px",
              display: "block",
              background: "white",
              marginBottom: "1rem",
            }}
          >
            Our Story...
          </Text>
          <Text
            fontSize={["20px", "24px", "24px"]}
            color="white"
            fontWeight={"bold"}
            letterSpacing="0.3px"
          >
            Once in a while, right in the middle of an ordinary life, love gives
            us a fairy tale.
          </Text>
        </VStack>
        <VStack
          w="full"
          align={"center"}
          pos="relative"
          style={{ margin: "2rem 0px" }}
        >
          <VStack w={["full", "80%"]} p="1rem">
            <VStack
              w="full"
              bg="#03A9F4"
              pos="relative"
              align={"center"}
              // left="15%"
              p="35px"
              rounded={"1.5rem"}
            >
              <Text
                fontSize="20px"
                fontWeight={900}
                color="white"
                _after={{
                  content: "' '",
                  width: "100%",
                  marginTop: "3px",
                  height: "2px",
                  display: "block",
                  background: "white",
                }}
              >
                FIRST WE MET
              </Text>
              <Text
                fontSize="17px"
                letterSpacing={"1px"}
                fontWeight={"bold"}
                color="white"
              >
                Wed,&nbsp; 25&nbsp; Dec,&nbsp; 2019
              </Text>
              <Text
                fontSize="14px"
                textAlign="center"
                fontWeight={"light"}
                color="white"
              >
                If we could only control the time,
                We would go back to the moment when we both first met, not to
                change anything, but to experience it all over again!
              </Text>
              <HStack
                bg="#03A9F4"
                justify={"Center"}
                w="100px"
                h="100px"
                pos="relative"
                bottom={0}
                roundedTop={"20px"}
                roundedBottomLeft={"20px"}
                zIndex={10}
              >
                <Icon as={GiLovers} color="white" width="35px" h="35px" />
                <Icon
                  as={TbCircleDotted}
                  color="white"
                  width="100px"
                  h="100px"
                  pos="absolute"
                  style={{ margin: "0px !important" }}
                />
              </HStack>
            </VStack>
          </VStack>
          <VStack w={["full", "80%"]} p="1rem">
            <VStack
              w="full"
              bg="#F57C00"
              pos="relative"
              p="35px"
              rounded={"1.5rem"}
            >
              <Text
                fontSize="20px"
                fontWeight={900}
                color="white"
                _after={{
                  content: "' '",
                  width: "100%",
                  marginTop: "3px",
                  height: "2px",
                  display: "block",
                  background: "white",
                }}
              >
                ENGAGEMENT
              </Text>
              <Text
                fontSize="17px"
                letterSpacing={"1px"}
                fontWeight={"bold"}
                color="white"
              >
                Sat,&nbsp; 11&nbsp; June,&nbsp; 2022
              </Text>
              <Text
                fontSize="14px"
                textAlign="center"
                fontWeight={"light"}
                color="white"
              >
                Some knots are meant to be tied forever. Here is our sweet and
                smooth journey towards eternity holding each other's hand. Two
                souls but a single thought; two hearts that started beating as
                one.
              </Text>
              <HStack
                bg="#F57C00"
                justify={"Center"}
                w="100px"
                h="100px"
                pos="relative"
                bottom={0}
                roundedTop={"20px"}
                roundedBottomLeft={"20px"}
                zIndex={10}
              >
                <Icon as={GiDiamondRing} color="white" width="35px" h="35px" />
                <Icon
                  as={TbCircleDotted}
                  color="white"
                  width="100px"
                  h="100px"
                  pos="absolute"
                  style={{ margin: "0px !important" }}
                />
              </HStack>
            </VStack>
          </VStack>

          <VStack w={["full", "80%"]} p="1rem">
            <VStack
              w="full"
              bg="#D6136A"
              pos="relative"
              p="35px"
              rounded={"1.5rem"}
            >
              <Text
                fontSize="20px"
                fontWeight={900}
                color="white"
                _after={{
                  content: "' '",
                  width: "100%",
                  marginTop: "3px",
                  height: "2px",
                  display: "block",
                  background: "white",
                }}
              >
                WEDDING
              </Text>
              <Text
                fontSize="17px"
                letterSpacing={"1px"}
                fontWeight={"bold"}
                color="white"
              >
                Fri,&nbsp; 25&nbsp; Nov,&nbsp; 2022
              </Text>
              <Text
                fontSize="14px"
                textAlign="center"
                fontWeight={"light"}
                color="white"
              >
                The best part of the life is spending each and every day with
                the fiance, Going on a date, shopping wedding dress,
                preparing wedding vows with the excitement and eagerness of
                waiting for the big day!
              </Text>
              <HStack
                bg="#D6136A"
                justify={"Center"}
                w="100px"
                h="100px"
                pos="relative"
                bottom={0}
                roundedTop={"20px"}
                roundedBottomLeft={"20px"}
                zIndex={10}
              >
                <Icon as={GiSelfLove} color="white" width="35px" h="35px" />
                <Icon
                  as={TbCircleDotted}
                  color="white"
                  width="100px"
                  h="100px"
                  pos="absolute"
                  style={{ margin: "0px !important" }}
                />
              </HStack>
            </VStack>
          </VStack>
        </VStack>

        <Box
          w="80%"
          style={{ margin: "2rem 0px !important" }}
          h="100px"
          backgroundPosition={"center"}
          backgroundImage={
            "https://weddie.netlify.app/img/traditional_arch_border.png"
          }
          backgroundRepeat="repeat-x"
        />
      </VStack>
    </>
  );
};

export default Story;
