import { HStack, Icon, Text, VStack } from "@chakra-ui/react";
import React, { useState } from "react";
import { AiTwotoneHeart } from "react-icons/ai";
import { RiGalleryFill } from "react-icons/ri";
import { RiCalendarEventLine } from "react-icons/ri";
import { ImLocation2 } from "react-icons/im";
import { MdOutlineHistoryEdu } from "react-icons/md";
import { MdOutlineVideoLibrary } from "react-icons/md";
import { GiHamburgerMenu } from "react-icons/gi";
import { ImShare2 } from "react-icons/im";
import { useRouter } from "next/router";


const Nav = () => {
  const router=useRouter()
  const share=async()=>{
    console.log("prem")
    const shareData = {
      title: 'Chandni weds Rahul',
      text:"You are cordially invited to wedding of Chandni & Rahul",
      url: "https://chandni-rahul-marriage-invitation-premkumar5951.vercel.app/"
    }
    try{
      await navigator.share(shareData);
    }catch(e){
      console.log("something e")
    }

    setShow(false)
  }
  const [show,setShow]=useState(false)
  return (
    <>
      
      <HStack
        position={"sticky"}
        top="0px"
        w="full"
        h="70px"
        color="white"
        zIndex={1000}
        bg="#D6136A"
        align="center"
        py="1em" 
        px="2rem"
        
      >
        
        
     
        <HStack flex="2" >
          <Text fontSize={"20px"} fontWeight="semibold" letterSpacing={"0.7px"} opacity={show?"0":"1"}  transition="all 0.5s ease-in-out">
            Wedding Invitation
          </Text>
        </HStack>
        <HStack justify="flex-end" spacing={5} display={["none","none","flex"]}>
        <a href="#couple" className="w-full p-[0.5rem] cursor-pointer hover:bg-[#720a39]"><HStack w="full" >
            <Icon as={AiTwotoneHeart} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Couple
            </Text>
          </HStack></a>
          <a href="#story" className="w-full p-[0.5rem] cursor-pointer hover:bg-[#720a39]"><HStack  w="full"  >
            <Icon as={MdOutlineHistoryEdu} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Story
            </Text>
          </HStack></a>
          <a href="#events" className="w-full p-[0.5rem] cursor-pointer hover:bg-[#720a39]"><HStack  w="full"  >
            <Icon as={RiCalendarEventLine} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Events
            </Text>
          </HStack></a>
          <a href="#gallery" className="w-full p-[0.5rem] cursor-pointer hover:bg-[#720a39]"><HStack  w="full"  >
            <Icon as={RiGalleryFill} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Gallery
            </Text>
          </HStack></a>
          <a href="#video" className="w-full p-[0.5rem] cursor-pointer hover:bg-[#720a39]"><HStack  w="full"  >
            <Icon as={MdOutlineVideoLibrary} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Video
            </Text>
          </HStack></a>
          <a href="#location" className="w-full cursor-pointer p-[0.5rem] hover:bg-[#720a39]"><HStack  w="full"  >
            <Icon as={ImLocation2} w="20px" h="20px" />
            <Text
              className="m-0"
              fontSize="16px"
              fontWeight="semibold"
              letterSpacing={"0.7px"}
            >
              Location
            </Text>
          </HStack></a>
        </HStack>
        <Icon zIndex={1000} as={GiHamburgerMenu} w="25px" h="25px" pos="absolute" right="5%" display={["block","block","none"]} cursor="pointer" onClick={()=>{setShow(!show)}}/>
        <VStack  w="full" bg="#D6136A" zIndex={-10} color="white" pos="absolute" left="0px" top={show?"calc(0% + 60px)":"-700%"} opacity={show?"1":"0"} display={["flex","flex","none"]}  transition="all 0.5s ease-in-out" style={{"margin":"0px !important"}}  >
      
        <VStack align="flex-start" w="full" zIndex={"5"}>
        <a href="#couple" className="w-full p-[1rem] cursor-pointer hover:bg-[#720a39]" onClick={()=>{setShow(false)}}><HStack w="full" onClick={()=>{setShow(false)}}>
            <Icon as={AiTwotoneHeart} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Couple
            </Text>
          </HStack></a>
          <a href="#story" className="w-full p-[1rem] cursor-pointer hover:bg-[#720a39]" onClick={()=>{setShow(false)}}><HStack  w="full" onClick={()=>{setShow(false)}} >
            <Icon as={MdOutlineHistoryEdu} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Story
            </Text>
          </HStack></a>
          <a href="#events" className="w-full p-[1rem] cursor-pointer hover:bg-[#720a39]" onClick={()=>{setShow(false)}}><HStack  w="full" onClick={()=>{setShow(false)}} >
            <Icon as={RiCalendarEventLine} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Events
            </Text>
          </HStack></a>
          <a href="#gallery" className="w-full p-[1rem] cursor-pointer hover:bg-[#720a39]" onClick={()=>{setShow(false)}}><HStack  w="full" onClick={()=>{setShow(false)}} >
            <Icon as={RiGalleryFill} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Gallery
            </Text>
          </HStack></a>
          <a href="#video" className="w-full p-[1rem] cursor-pointer hover:bg-[#720a39]" onClick={()=>{setShow(false)}}><HStack  w="full" onClick={()=>{setShow(false)}} >
            <Icon as={MdOutlineVideoLibrary} w="20px" h="20px" />
            <Text fontSize="16px" fontWeight="semibold" letterSpacing={"0.7px"}>
              Video
            </Text>
          </HStack></a>
          <a href="#location" className="w-full p-[1rem] cursor-pointer hover:bg-[#720a39]" onClick={()=>{setShow(false)}}><HStack  w="full" onClick={()=>{setShow(false)}} >
            <Icon as={ImLocation2} w="20px" h="20px" />
            <Text
              className="m-0"
              fontSize="16px"
              fontWeight="semibold"
              letterSpacing={"0.7px"}
            >
              Location
            </Text>
          </HStack></a>
         <HStack p="1rem" cursor={"pointer"} _hover={{bg:'#720a39'}} w="full" onClick={()=>{share()}} >
            <Icon as={ImShare2} w="20px" h="20px" />
            <Text
              className="m-0"
              fontSize="16px"
              fontWeight="semibold"
              letterSpacing={"0.7px"}
            >
              Share
            </Text>
          </HStack>
        </VStack>
</VStack>
        
      </HStack>
    </>
  );
};

export default Nav;
