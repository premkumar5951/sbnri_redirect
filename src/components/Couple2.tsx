import { Box, HStack, Image, Text, VStack } from "@chakra-ui/react";
import React from "react";

const Couple2 = () => {
  return (
    <>
      <VStack w="full" align="center" id="couple">
        <Text
          fontSize={["30px","40px","40px"]}
          color="white"
          fontWeight={"bold"}
          letterSpacing="0.3px"
          _after={{
            content: "' '",
            width: "100%",
            marginTop: "3px",
            height: "2px",
            display: "block",
            background: "white",
          }}
        >
          Wedding Couple
        </Text>

        <HStack
          w="full"
          justify={"center"}
          style={{ margin: "1rem 0px !important" }}
          p="1rem"
          flexWrap={"wrap"}
        >
          
          <VStack
            w="315px"
            maxW={"315px"}
            h="450px"
            bg="#AF004D"
            rounded={"4px"}
            pos="relative"
            style={{"margin":"1rem 0.5rem !important"}}
          >
            <Image
              src="https://res.cloudinary.com/dxjehekla/image/upload/v1666209319/marriage/Rectangle_1_uzfw7r.png"
              roundedTop={"4px"}
            />
            <VStack
              w="full"
              pos="absolute"
              align="flex-end"
              right="0px"
              p="1rem"
            >
              <Text
                fontSize="20px"
                color="white"
                fontWeight={"bold"}
                letterSpacing="0.3px"
              >
                Chandni Sah
              </Text>
              <Text
                fontSize="13px"
                color="white"
                fontWeight={"bold"}
                letterSpacing="0.3px"
                style={{ margin: "0px" }}
              >
                Bride
              </Text>
            </VStack>
            <Box
              shadow="0 0 15px rgb(0 0 0 / 30%)"
              w="84px"
              h="84px"
              bg="black"
              rounded="full"
              position="relative"
              left={"-30%"}
              top="-30%"
              backgroundImage={
                "https://res.cloudinary.com/dxjehekla/image/upload/v1666211415/marriage/Picsart_22-10-20_01-59-30-340_b6chu2.jpg"
              }
              backgroundSize="cover"
              backgroundPosition={"top"}
              backgroundRepeat={"no-repeat"}
            />
            <Box
              w="full"
              p="1.5rem"
              fontSize={"16px"}
              pos="absolute"
              bottom="10%"
              letterSpacing={"1.8px"}
              color="white"
            >
              <Text>
                A Beautiful woman with a fine prospect of happiness behind her.
                There is something magical about becoming the bride. There is
                something special about the commitment of spending the eternity
                with the man she loves.
              </Text>
            </Box>
            <Image w="80px" right="5%" pos="absolute" bottom="-10px" src="https://res.cloudinary.com/dxjehekla/image/upload/v1666215429/marriage/Get_Quote_cbzhxr.png"/>

          </VStack>
          <VStack
            w="315px"
            h="450px"
            bg="#0288D1"
            rounded={"4px"}
            pos="relative"
            style={{"margin":"1rem 0.5rem !important"}}

          >
            <Image
              src="https://res.cloudinary.com/dxjehekla/image/upload/v1666209319/marriage/Rectangle_1_1_tq9mrx.png"
              roundedTop={"4px"}
            />
            <VStack
              w="full"
              pos="absolute"
              align="flex-end"
              right="0px"
              p="1rem"
            >
              <Text
                fontSize="20px"
                color="white"
                fontWeight={"bold"}
                letterSpacing="0.3px"
              >
                Rahul Singh
              </Text>
              <Text
                fontSize="13px"
                color="white"
                fontWeight={"bold"}
                letterSpacing="0.3px"
                style={{ margin: "0px" }}
              >
                Bridegroom
              </Text>
            </VStack>
            <Box
              shadow="0 0 15px rgb(0 0 0 / 30%)"
              w="84px"
              h="84px"
              bg="black"
              rounded="full"
              position="relative"
              left={"-30%"}
              top="-30%"
              backgroundImage={
                "https://res.cloudinary.com/dxjehekla/image/upload/v1666211415/marriage/Picsart_22-10-20_01-59-00-347_raebye.jpg"
              }
              backgroundSize="cover"
              backgroundPosition={"top"}
              backgroundRepeat={"no-repeat"}
            />
            <Box
              w="full"
              p="1.5rem"
              fontSize={"16px"}
              pos="absolute"
              bottom="17%"
              letterSpacing={"1.8px"}
              color="white"
            >
              <Text>
                A Charismatic young man who always smiles proudly because he is
                convinced that he has accomplished something quite wonderful by
                persuading the woman of his dreams to marry him.
              </Text>
            </Box>
            <Image w="80px" right="5%" pos="absolute" bottom="-10px" src="https://res.cloudinary.com/dxjehekla/image/upload/v1666215429/marriage/Get_Quote_cbzhxr.png"/>

          </VStack>
        </HStack>

        {/* <Box
          w="450px"
          h="450px"
          mb="2rem"
          padding="0rem"
          border="25px solid transparent"
          style={{
            borderImageSlice: "43 45 45 41",
            borderImageWidth: "25px 25px 25px 25px",
            borderImageRepeat: "round round",
            borderImageSource:
              "url('https://weddie.netlify.app/img/traditional_wedding_header_frame.png')",
            borderStyle: "solid",
          }}
          __css={{
            "--webkit-border-image":
              "url(https://weddie.netlify.app/img/traditional_wedding_header_frame.png) 45 round",
          }}
        >
          <Image
            w="full"
            h="full"
            src="https://res.cloudinary.com/dxjehekla/image/upload/v1666204504/marriage/WhatsApp_Image_2022-10-19_at_11.30.19_PM_qcnyg7.jpg"
            objectFit={"cover"}
            objectPosition="top"
          />
        </Box> */}

        <Box
          w="80%"
          style={{ margin: "2rem 0px !important" }}
          h="100px"
          backgroundPosition={"center"}
          backgroundImage={
            "https://weddie.netlify.app/img/traditional_arch_border.png"
          }
          backgroundRepeat="repeat-x"
        />
      </VStack>
    </>
  );
};

export default Couple2;
