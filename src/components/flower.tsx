import {
    Box,
    HStack,
    Icon,
    Image,
    AspectRatio,
    Text,
    VStack,
  } from "@chakra-ui/react";
  import React from "react";
  import { GiLovers } from "react-icons/gi";
  import { TbCircleDotted } from "react-icons/tb";
  import { GiSelfLove } from "react-icons/gi";
  import { GiDiamondRing } from "react-icons/gi";
  
  const Flower = () => {
    return (
      <>
      <link rel="stylesheet" href="/css/animate.module.css" />
  
  <HStack w="full" justify="space-between" position={"sticky"} align="center" className="flowers" top={"-100px"} flexWrap="wrap">

<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="45px" h="45px" style={{animationDelay:"5s"}} opacity="1"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="35px" h="35px" style={{animationDelay:"7.3s"}} opacity="0.8"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="10px" h="10px" style={{animationDelay:"3s"}} opacity="0.5"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="40px" h="40px" style={{animationDelay:"5s"}} opacity="0.9"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="30px" h="30px" style={{animationDelay:"2s"}} opacity="0.75"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="15px" h="15px" style={{animationDelay:"16s"}} opacity="0.55"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="20px" h="20px" style={{animationDelay:"18.5s"}} opacity="0.6"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="45px" h="45px" style={{animationDelay:"9.5s"}} opacity="1"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="35px" h="35px" style={{animationDelay:"20s"}} opacity="0.8"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="10px" h="10px" style={{animationDelay:"12.3s"}} opacity="0.5"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="25px" h="25px" style={{animationDelay:"26.2s"}} opacity=""/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" w="40px" h="40px" style={{animationDelay:"16.6s"}} opacity="0.9"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]}  w="30px" h="30px" style={{animationDelay:"23.7s"}} opacity="0.75"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]}  w="15px" h="15px" style={{animationDelay:"10.5s"}} opacity="0.55"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]}  w="20px" h="20px" style={{animationDelay:"3.5s"}} opacity="0.6" />
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]}  w="45px" h="45px" style={{animationDelay:"5s"}} opacity="1"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="35px" h="35px" style={{animationDelay:"20s"}} opacity="0.8"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="10px" h="10px" style={{animationDelay:"12.3s"}} opacity="0.5"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="25px" h="25px" style={{animationDelay:"3.2s"}} opacity=""/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="40px" h="40px" style={{animationDelay:"16.6s"}} opacity="0.9"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="30px" h="30px" style={{animationDelay:"1.7s"}} opacity="0.75"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="15px" h="15px" style={{animationDelay:"10.5s"}} opacity="0.55"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="20px" h="20px" style={{animationDelay:"3.5s"}} opacity="0.6" />
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="45px" h="45px" style={{animationDelay:"5s"}} opacity="1"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="35px" h="35px" style={{animationDelay:"1.3s"}} opacity="0.8"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="10px" h="10px" style={{animationDelay:"3s"}} opacity="0.5"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="40px" h="40px" style={{animationDelay:"5s"}} opacity="0.9"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="30px" h="30px" style={{animationDelay:"2s"}} opacity="0.75"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="15px" h="15px" style={{animationDelay:"16s"}} opacity="0.55"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="20px" h="20px" style={{animationDelay:"18.5s"}} opacity="0.6"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="45px" h="45px" style={{animationDelay:"9.5s"}} opacity="1"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="35px" h="35px" style={{animationDelay:"20s"}} opacity="0.8"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="10px" h="10px" style={{animationDelay:"12.3s"}} opacity="0.5"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="25px" h="25px" style={{animationDelay:"3.2s"}} opacity=""/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="40px" h="40px" style={{animationDelay:"16.6s"}} opacity="0.9"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="30px" h="30px" style={{animationDelay:"28.7s"}} opacity="0.75"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="15px" h="15px" style={{animationDelay:"10.5s"}} opacity="0.55"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="20px" h="20px" style={{animationDelay:"3.5s"}} opacity="0.6" />
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="45px" h="45px" style={{animationDelay:"5s"}} opacity="1"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="35px" h="35px" style={{animationDelay:"1.3s"}} opacity="0.8"/>
<Image src="https://res.cloudinary.com/dxjehekla/image/upload/v1666389227/marriage/Group_1_2_wezzlm.png" display={["none","none","block"]} w="10px" h="10px" style={{animationDelay:"3s"}} opacity="0.5"/>
  </HStack>
        
      </>
    );
  };
  
  export default Flower;
  