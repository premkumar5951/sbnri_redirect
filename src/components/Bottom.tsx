import {
    Box,
    HStack,
    Icon,
    Image,
    AspectRatio,
    Text,
    VStack,
  } from "@chakra-ui/react";
  import React from "react";
  import { GiLovers } from "react-icons/gi";
  import { TbCircleDotted } from "react-icons/tb";
  import { GiSelfLove } from "react-icons/gi";
  import { GiDiamondRing } from "react-icons/gi";
  import { ImShare2 } from "react-icons/im";
import { useRouter } from "next/router";

  const Bottom = () => {
  const router=useRouter()

    const share=async()=>{
      console.log("prem")
      const shareData = {
        title: 'Chandni weds Rahul',
        text:"You are cordially invited to wedding of Chandni & Rahul.",
        url: "https://chandni-rahul-marriage-invitation-premkumar5951.vercel.app/"
      }
      try{
        await navigator.share(shareData);
      }catch(e){
        console.log("something e")
      }
  
    }
    return (
      <>
  
        <VStack w="full" align="center" h="50px" bg="#AB0F55">
          <HStack  justify={"Center"} p="0.5rem" onClick={()=>{share()}} cursor="pointer">
<Text textAlign={"center"} fontSize="16px" color="white" fontWeight={"normal"} letterSpacing="1px"> Share</Text>
<Icon as={ImShare2} w="16px" h="16px" color="white"/>
          </HStack>
        </VStack>
      </>
    );
  };
  
  export default Bottom;
  