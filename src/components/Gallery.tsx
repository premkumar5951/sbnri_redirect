import { Box, HStack, Icon, Image, Text, VStack } from "@chakra-ui/react";
import React from "react";
import { GiLovers } from "react-icons/gi";
import { TbCircleDotted } from "react-icons/tb";
import { GiSelfLove } from "react-icons/gi";
import { GiDiamondRing } from "react-icons/gi";

const Gallery = () => {
  return (
    <>
      <link rel="stylesheet" href="/css/animate.module.css" />

      <VStack w="full" align="center" id="gallery">
        <VStack w={["90%", "80%"]} textAlign={"center"} >
          <Text
            fontSize={["30px", "40px", "40px"]}
            color="white"
            fontWeight={"bold"}
            letterSpacing="0.3px"
            _after={{
              content: "' '",
              width: "100%",
              marginTop: "3px",
              height: "2px",
              display: "block",
              background: "white",
            marginBottom:"1rem"

            }}
          >
            Photo Gallery
          </Text>
          <Text
                    fontSize={["20px","24px","24px"]}

            color="white"
            fontWeight={"bold"}
            letterSpacing="0.3px"
          >
            From this day forward, you shall not walk alone. My heart will be
            your Shelter, And my arms will be your Home...
          </Text>
        </VStack>

        <HStack
          w="90%"
          align="center"
          justify="center"
          style={{ margin: "2rem 0px" }}
          flexWrap="wrap"
          overflow={"hidden"}
        >
          <Box
            w="300px"
            h="300px"
            bg="red"
            overflow={"hidden"}
            className="animate-box"
            position="relative"
            bgImage={
              "https://res.cloudinary.com/dxjehekla/image/upload/v1666361167/marriage/WhatsApp_Image_2022-10-21_at_10.26.53_AM_w74azl.jpg"
            }
            bgRepeat="no-repeat"
            bgSize={"cover"}
            style={{ margin: "1rem 0.5rem !important" }}
          >
            <VStack
              w="full"
              h="70%"
              transition={"all 0.3s ease"}
              className="animate-inner"
              pos="absolute"
              bg="#00000063"
              bottom={"-35%"}
              align="flex-start"
              p="1rem"
            >
              <Text
                fontSize="20px"
                color="white"
                textAlign={"left"}
                fontWeight={"semibold"}
                letterSpacing="0.3px"
              >
                Chandni Sah
              </Text>
            </VStack>
          </Box>
         
          <Box
            w="300px"
            h="300px"
            bg="red"
            className="animate-box"
            position="relative"
            overflow={"hidden"}
            bgImage={
              "https://res.cloudinary.com/dxjehekla/image/upload/v1666361200/marriage/WhatsApp_Image_2022-10-21_at_10.26.55_AM_daw8mx.jpg"
            }
            bgRepeat="no-repeat"
            bgSize={"cover"}
            style={{ margin: "1rem 0.5rem !important" }}
          >
            <VStack
              w="full"
              h="70%"
              transition={"all 0.3s ease"}
              className="animate-inner"
              pos="absolute"
              bg="#00000063"
              bottom={"-35%"}
              align="flex-start"
              p="1rem"
            >
              <Text
                fontSize="20px"
                color="white"
                textAlign={"left"}
                fontWeight={"semibold"}
                letterSpacing="0.3px"
              >
                Rahul Singh
              </Text>
            </VStack>
          </Box>
          <Box
            w="300px"
            h="300px"
            bg="red"
            className="animate-box"
            position="relative"
            overflow={"hidden"}
            bgImage={
              "https://res.cloudinary.com/dxjehekla/image/upload/v1666361137/marriage/WhatsApp_Image_2022-10-21_at_10.26.53_AM_1_fgzt5d.jpg"
            }
            bgRepeat="no-repeat"
            bgSize={"cover"}
            style={{ margin: "1rem 0.5rem !important" }}
          >
            <VStack
              w="full"
              h="70%"
              transition={"all 0.3s ease"}
              className="animate-inner"
              pos="absolute"
              bg="#00000063"
              bottom={"-35%"}
              align="flex-start"
              p="1rem"
            >
              <Text
                fontSize="20px"
                color="white"
                textAlign={"left"}
                fontWeight={"semibold"}
                letterSpacing="0.3px"
              >
                 Chandni & Rahul
              </Text>
            </VStack>
          </Box>
          
        </HStack>

        {/* <Box
          w="450px"
          h="450px"
          mb="2rem"
          padding="0rem"
          border="25px solid transparent"
          style={{
            borderImageSlice: "43 45 45 41",
            borderImageWidth: "25px 25px 25px 25px",
            borderImageRepeat: "round round",
            borderImageSource:
              "url('https://weddie.netlify.app/img/traditional_wedding_header_frame.png')",
            borderStyle: "solid",
          }}
          __css={{
            "--webkit-border-image":
              "url(https://weddie.netlify.app/img/traditional_wedding_header_frame.png) 45 round",
          }}
        >
          <Image
            w="full"
            h="full"
            src="https://res.cloudinary.com/dxjehekla/image/upload/v1666204504/marriage/WhatsApp_Image_2022-10-19_at_11.30.19_PM_qcnyg7.jpg"
            objectFit={"cover"}
            objectPosition="top"
          />
        </Box> */}

        <Box
          w="80%"
          style={{ margin: "2rem 0px !important" }}
          h="100px"
          backgroundPosition={"center"}
          backgroundImage={
            "https://weddie.netlify.app/img/traditional_arch_border.png"
          }
          backgroundRepeat="repeat-x"
        />
      </VStack>
    </>
  );
};

export default Gallery;
