import {
  Box,
  HStack,
  Icon,
  Image,
  AspectRatio,
  Text,
  VStack,
} from "@chakra-ui/react";
import React from "react";
import { GiLovers } from "react-icons/gi";
import { TbCircleDotted } from "react-icons/tb";
import { GiSelfLove } from "react-icons/gi";
import { GiDiamondRing } from "react-icons/gi";

const Map = () => {
  return (
    <>
      <link rel="stylesheet" href="/css/animate.module.css" />

      <VStack w="full" align="center" id="location" zIndex={100}>
        <Text
          fontSize={["30px","40px","40px"]}
          color="white"
          fontWeight={"bold"}
          letterSpacing="0.3px"
          mb="2rem"
          _after={{
            content: "' '",
            width: "100%",
            marginTop: "3px",
            height: "2px",
            display: "block",
            background: "white",
          }}
        >
          Wedding Location
        </Text>

        
          <iframe
            title="wedding Invitation"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3608.8621250740725!2d87.6422541!3d25.2415684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39fa9894ad1fe3d1%3A0xee75f11063ce8cdf!2sNirmaya%20Cinema%20Hall!5e0!3m2!1sen!2sin!4v1666305628444!5m2!1sen!2sin"
            allowFullScreen
            width="100%"
            height="500px"
          />
      </VStack>
    </>
  );
};

export default Map;
