import {
  Box,
  HStack,
  Icon,
  Image,
  AspectRatio,
  Text,
  VStack,
} from "@chakra-ui/react";
import React from "react";
import { GiLovers } from "react-icons/gi";
import { TbCircleDotted } from "react-icons/tb";
import { GiSelfLove } from "react-icons/gi";
import { GiDiamondRing } from "react-icons/gi";

const Video = () => {
  return (
    <>
      <link rel="stylesheet" href="/css/animate.module.css" />

      <VStack w="full" align="center" id="video">
        <Text
          fontSize={["30px","40px","40px"]}
          color="white"
          fontWeight={"bold"}
          letterSpacing="0.3px"
          mb="2rem"
          _after={{
            content: "' '",
            width: "100%",
            marginTop: "3px",
            height: "2px",
            display: "block",
            background: "white",
          }}
        >
          Invitation Video
        </Text>

        <AspectRatio width="full" maxW={["full","80%","60%"]} ratio={16 / 9}>
          <Box
            as="iframe"
            title="wedding Invitation"
            src="https://www.youtube.com/embed/AjTm35y-YSE"
            ng-show="showvideo"
            allowFullScreen
          />
        </AspectRatio>
        <Box
          w="80%"
          style={{ margin: "2rem 0px !important" }}
          h="100px"
          backgroundPosition={"center"}
          backgroundImage={
            "https://weddie.netlify.app/img/traditional_arch_border.png"
          }
          backgroundRepeat="repeat-x"
        />
      </VStack>
    </>
  );
};

export default Video;
