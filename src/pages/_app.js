import '../styles/globals.css'

// pages/_app.js
import { ChakraProvider } from '@chakra-ui/react'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  return (
    <>
    <Head>
    <meta property="og:title" content="Chandni weds Rahul"/>
<meta property="og:site_name" content="Chandni weds Rahul"/>
<meta property="og:url" content="https://chandni-rahul-marriage-invitation-premkumar5951.vercel.app"/>
<meta property="og:description" content="You are cordially invited to wedding of Chandni & Rahul"/>
<meta property="og:type" content=""/>
<meta property="og:image" content="https://res.cloudinary.com/dxjehekla/image/upload/v1666302809/marriage/WhatsApp_Image_2022-10-20_at_1.14.35_AM_i0soby.jpg"/>
    </Head>
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
    </>
  )
}

export default MyApp